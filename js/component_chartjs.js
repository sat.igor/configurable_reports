M.block_component_chartjs = {

    init: function(Y, id,type,labels,datasets,options) {

		Chart.pluginService.register({
			beforeDraw: function (chart, easing) {
				if (chart.config.options.chartArea && chart.config.options.chartArea.backgroundColor) {
					var helpers = Chart.helpers;
					var ctx = chart.chart.ctx;
					var chartArea = chart.chartArea;

					ctx.save();
					ctx.fillStyle = chart.config.options.chartArea.backgroundColor;
					ctx.fillRect(0, 0, 10000, 10000);
					ctx.restore();
				}
			}
		});
        // The code below is needed for the graph to be displayed in the template.
        // The header field does not allow canvas output.
        // see blocks/configurable_reports/report.class.php:680
        $('.'+id).append('<canvas id="'+id+'"></canvas>');
        var canvas = document.getElementById(id);
        var ctx = document.getElementById(id);
        new Chart(ctx, {
            "type": type,
            backgroundColor: "#F5DEB3",
            "data": {
                "labels": labels,
                "datasets": datasets
            },
            "options": options
        });
        
    },
    downloadpdf: function(Y) {

        var pdf = new jsPDF('', 'pt', 'a4');
        var title = $('.page-header-headings h1').text();
        var position = 0;
        var x = 20;
        var y = 20;
        var lMargin=15; //left margin in mm
        var rMargin=15; //right margin in mm
        var pdfInMM=210;  // width of A4 in mm
        var pageCenter=pdf.internal.pageSize.width/2;

        var pageHeight = 841.89 - y;

        var xOffsettext = (pdf.internal.pageSize.width / 2) - (pdf.getStringUnitWidth(title) * pdf.internal.getFontSize() / 2);
        var lines =pdf.splitTextToSize(title, (pdf.internal.pageSize.width-lMargin-rMargin));
        var dim = pdf.getTextDimensions('Text');
        var lineHeight = dim.h
        for(var i=0;i<lines.length;i++){
          lineTop = (lineHeight/2)*i
          pdf.text(lines[i],pageCenter,y,'center'); //see this line
          y += 20;
        }
        
        $("#printablediv canvas").each(function(index) {
            var canvas = document.getElementById($(this).attr('id'));

            var contentWidth = canvas.width;
            var contentHeight = canvas.height;

            var imgWidth = 595.28 - (x * 2);
            var imgHeight = 592.28 / contentWidth * contentHeight;

            var pageData = canvas.toDataURL('image/jpg');

            if ((y + imgHeight) < pageHeight) {
                pdf.addImage(pageData, 'JPEG', x, y, imgWidth, imgHeight);
            } else {
                pdf.addPage();

                    y = 20;
                    pdf.addImage(pageData, 'JPEG', x, y, imgWidth, imgHeight)
            }

            position++;
            y += imgHeight;
        });
        //export table
        // pdf.addPage();
        // y = 20;
        // pdf.fromHTML($('#printablediv')[0],x,y,{ // y coord
        //     'width': pageHeight
        // },);
        pdf.save('content.pdf');


    }
}

