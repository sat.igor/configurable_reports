<?php
// This file is part of Moodle - http://moodle.org/
//
// Moodle is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// Moodle is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with Moodle.  If not, see <http://www.gnu.org/licenses/>.

/**
 * Configurable Reports
 * A Moodle block for creating customizable reports
 * @package blocks
 * @author: Juan leyva <http://www.twitter.com/jleyvadelgado>
 * @date: 2009
 */

require_once($CFG->dirroot.'/blocks/configurable_reports/plugin.class.php');

class plugin_chartjsbar extends plugin_base {

    public function init() {
        $this->fullname = "Bar chart";
        $this->form = true;
        $this->ordering = true;
        $this->reporttypes = array('courses', 'sql', 'users', 'timeline', 'categories');
    }

    public function summary($data) {
        return "Bar chart summary";
    }

    // Data -> Plugin configuration data.
    public function execute($id, $data, $finalreport) {
        global $DB, $CFG;
        if ($finalreport) {
            $color = [];
            list($chartlabelid, $chartlabelname) = explode(",", $data->label_field);
            $chartvaluearray = [];
            if (is_array($data->value_fields)){
                foreach ($data->value_fields as $field){
                    list($tempid, $tempname) = explode(",", $field);
                    $chartvaluearray[$tempid] = format_string($tempname);
                }
            }
            foreach ($finalreport as $item){
                foreach ($item as $key => $value){
                    $array[$key][] = format_string($value);
                    $color[$key] = "rgba(".rand(0,255).", ".rand(0,255).", ".rand(0,255).", 0.5)";
                }
            }
            foreach ($chartvaluearray as $key => $value) {
                $datasets[] = ['label' => format_string($value),
                        'data' => $array[$key],
                        'fill' => false,
                        'backgroundColor' => $color[$key],
                        'borderWidth' => 1];
            }
        }
        $options = ['scales' => [
                'yAxes' => [['stacked' => ($data->stacked)?true:false]],
                'xAxes' => [['stacked' => ($data->stacked)?true:false]]
        ],
        'chartArea'=>['backgroundColor'=>'rgba(255, 255, 255, 1)']];
        $style = 'display: inline-block; ';
        $style .= ($data->width)?" width:{$data->width}px;":'';
        $style .= ($data->height)?" height:{$data->height}px;":'';
        $return = [
                'id' => strtolower("chart".$id),
                'type' => $data->type,
                'labels' => $array[$chartlabelid],
                'datasets' => $datasets,
                'options' => $options,
                'style' => $style
        ];
        return $return;

        }

    public function get_series($data) {
        $graphdataraw = required_param('graphdata', PARAM_RAW);
        $graphdata = json_decode(urldecode($graphdataraw));
        return (array)$graphdata;
    }
}
